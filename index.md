# Portfolio

---

## Machine Learning

### 2.5D Visual Sound:
[![View on GitHub](https://img.shields.io/badge/GitHub-View_on_GitHub-blue?logo=GitHub)](https://github.com/dasdristanta13/2.5D-Visual-Sound)


**Description:** 
<div style="text-align: justify">Binaural audio provides a listener with 3D sound sensation, allowing a rich perceptual experience of the scene.However, binaural recordings are scarcely available and require nontrivial expertise and equipment to obtain. We propose to convert common monaural audio into binaural audio by leveraging video.</div>

<center><img src="images/ML_Project_report.png?raw=true"/></center>
---

## Computer Vision

### Hybrid Image Formation:
[![View on GitHub](https://img.shields.io/badge/GitHub-View_on_GitHub-blue?logo=GitHub)](https://github.com/dasdristanta13/Computer_Vision/tree/main/Hybrid%20Image%20Formation)

**Description:** 
<div style="text-align: justify">Hybrid images are images with two interpretation. The interpretation changes with viewing distance. With two suitable images these can be created.One of the superimposing images has to be in low frequency(which will be generated by applying appropriate filters).</div>

**Example:**

<img src="images/bicycle.bmp?raw=true" width="240" height="150"/><img src="images/motorcycle.bmp?raw=true" width="240" height="150"/>

<center><img src="images/hybrid_image_scales3.jpg?raw=true" height="150"/></center>

---
### Harris Corner Detection and SIFT:
[![View on GitHub](https://img.shields.io/badge/GitHub-View_on_GitHub-blue?logo=GitHub)](https://github.com/dasdristanta13/Computer_Vision/tree/main/Harris_Corner_Detection_And_SIFT)

**Description:** 
<div style="text-align: justify">The Harris Corner detector is a standard technique for locating interest points on an image.Despite the appearance of many feature detectors in the last decade, it continues to be reference technique, which is typically used for camera caliberation,image matching, tracking or video stabilization.</div>

**Example:**

<center><img src="images/matched_home.png?raw=true" height="150"/></center>

---
### Hough Line and Circle Detector:
[![View on GitHub](https://img.shields.io/badge/GitHub-View_on_GitHub-blue?logo=GitHub)](https://github.com/dasdristanta13/Computer_Vision/tree/main/Hough_Line_And_Circle_Detector)

**Description:**

**Example:**

<img src="images/input1.png?raw=true" height="150"/><img src="images/finalLinesAndCircles.png?raw=true" height="150"/>

---
### Camera Caliberation and Fundamental Matrix:
[![View on GitHub](https://img.shields.io/badge/GitHub-View_on_GitHub-blue?logo=GitHub)](https://github.com/dasdristanta13/Computer_Vision/tree/main/Camera_Caliberation_and_Fundamental_Matrix)

**Description:**

The objective of this project was to improve upon image matching by leveraging epipolar geometry of a stereo image pair while applying RANSAC to reject poorly matched feature points. After performing a simple camera calibration, SIFT feature points detected in an image pair were used to estimate the Fundamental Matrix; using the estimation of the Fundamental Matrix, RANSAC was used to match the image pair. The following sections describe the approach and highlight the results of the project.

**Example:**


## Exploratory Data Analysis

### Fifa 21 game data analysis
[![View on GitHub](https://img.shields.io/badge/GitHub-View_on_GitHub-blue?logo=GitHub)](https://github.com/dasdristanta13/Fifa21EDA)

**Description:**

This project focuses on visualizing the Fifa 21 game data and getting inference out of that.Data includes latest edition FIFA 2021 players attributes like Age, Nationality, Overall, Potential, Club, Value, Wage, Preferred Foot, International Reputation, Weak Foot, Skill Moves, Work Rate, Position, Jersey Number, Joined, Loaned From, Contract Valid Until, Height, Weight etc.

### Zomato food data analysis
[![View on Github](https://img.shields.io/badge/GitHub-View_on_GitHub-blue?logo=GitHub)](https://github.com/dasdristanta13/Zomato_Food_EDA)

**Description**

This project focuses on visualizing the Zomato food data and getting inference out of it both using R and Python.

## Statistical Data Analysis

### Linear Discriminant Analysis of a Bankruptcy Dataset
[![View on GitHub](https://img.shields.io/badge/GitHub-View_on_GitHub-blue?logo=GitHub)](https://github.com/dasdristanta13/Bankruptcy_LDA)

**Description**

This project majorly focuses on statistical data analysis(**LDA**) of a bankruptcy data.
